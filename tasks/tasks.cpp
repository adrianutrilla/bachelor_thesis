#include <hpx/hpx.hpp>
#include <hpx/hpx_init.hpp>

#include <hpx/runtime/resource/partitioner.hpp>
#include <hpx/runtime/threads/detail/scheduled_thread_pool_impl.hpp>
#include <hpx/runtime/threads/executors/default_edf_executor.hpp>
#include <hpx/runtime/threads/executors/pool_executor.hpp>
#include <hpx/runtime/threads/policies/edf_scheduler.hpp>

#include <hpx/include/iostreams.hpp>
#include <hpx/include/runtime.hpp>

#include <chrono>
#include <vector>

using edf_scheduler = hpx::threads::policies::edf_scheduler<hpx::util::spinlock>;

static constexpr std::chrono::steady_clock::duration RUNTIME =
    std::chrono::seconds(10);

static constexpr std::chrono::steady_clock::duration MAX_WAIT_INTERVAL =
    std::chrono::microseconds(25);

void yield_maybe_synchronized() {
  hpx::threads::thread_data *next_thread = nullptr;
  auto self = hpx::this_thread::get_id().native_handle().get();
  auto scheduler = dynamic_cast<edf_scheduler *>(self->get_scheduler_base());
  scheduler->peek_next_thread(next_thread);
  if (next_thread != NULL && self != NULL) {
    if (next_thread->get_deadline() < self->get_deadline()) {
      hpx::this_thread::yield();
    }
  }
}

void yield_maybe() {
  auto self = hpx::this_thread::get_id().native_handle().get();
  auto scheduler = dynamic_cast<edf_scheduler *>(self->get_scheduler_base());
  auto next_deadline = scheduler->peek_next_deadline();
  if (self != NULL) {
    if (next_deadline < self->get_deadline()) {
      hpx::this_thread::yield();
    }
  }
}

void work_for(std::chrono::steady_clock::duration duration) {
  while (duration > MAX_WAIT_INTERVAL) {
    auto start = std::chrono::steady_clock::now();
    auto wait_until = start + MAX_WAIT_INTERVAL;
    while (std::chrono::steady_clock::now() <= wait_until)
      ;
    yield_maybe();
    auto after_yield = std::chrono::steady_clock::now();
    duration -= after_yield - start;
  }
  auto wait_until = std::chrono::steady_clock::now() + duration;
  while (std::chrono::steady_clock::now() <= wait_until)
    ;
}

enum class TaskTimingEvent {
  ACTIVATION,
  WORK_STARTED,
  WORK_DONE,
};

struct TaskTiming {
  TaskTimingEvent event;
  std::chrono::steady_clock::time_point time;
  std::size_t activation;
};

class Task {
public:
  Task(std::string name, std::string pool_name,
       std::chrono::steady_clock::duration activation_interval,
       std::chrono::steady_clock::duration work_duration)
      : name_(name), pool_name_(pool_name),
        activation_interval_(activation_interval),
        work_duration_(work_duration),
        timer_([this]() { return this->do_work(); }, activation_interval) {
    timings_.reserve(3 * (RUNTIME / activation_interval));
  }

  Task(Task &&rhs)
      : name_(rhs.name_), pool_name_(rhs.pool_name_),
        activation_interval_(rhs.activation_interval_),
        work_duration_(rhs.work_duration_),
        timer_([this]() { return this->do_work(); }, rhs.activation_interval_) {
  }

  std::vector<TaskTiming> get_timings() { return timings_; }

  std::string get_name() { return name_; }

  void start() {
    current_activation_ = 0;
    timer_.start();
  }

  void stop() { timer_.stop(); }

protected:
  bool do_work() {
    auto activation = this->current_activation_;
    this->current_activation_++;
    timings_.push_back(TaskTiming{
        TaskTimingEvent::ACTIVATION,
        std::chrono::steady_clock::now(),
        activation,
    });
    auto deadline = get_next_deadline();
    hpx::threads::executors::pool_executor executor(pool_name_, deadline);
    hpx::async(executor, [this, activation, deadline]() {
      timings_.push_back(TaskTiming{
          TaskTimingEvent::WORK_STARTED,
          std::chrono::steady_clock::now(),
          activation,
      });
      work();
      auto done_time = std::chrono::steady_clock::now();
      timings_.push_back(TaskTiming{
          TaskTimingEvent::WORK_DONE,
          done_time,
          activation,
      });
    });
    return true;
  }

  std::string name_;
  std::string pool_name_;
  std::size_t current_activation_;
  std::chrono::steady_clock::duration activation_interval_;
  std::chrono::steady_clock::duration work_duration_;
  hpx::util::interval_timer timer_;
  std::vector<TaskTiming> timings_;
  virtual std::chrono::steady_clock::time_point get_next_deadline() = 0;
  virtual void work() = 0;
};

class DeadlineTask : public Task {
public:
  DeadlineTask(std::string name, std::string pool_name,
               std::chrono::steady_clock::duration activation_interval,
               std::chrono::steady_clock::duration work_duration,
               std::chrono::steady_clock::duration relative_deadline)
      : Task(name, pool_name, activation_interval, work_duration),
        relative_deadline_(relative_deadline) {}

  DeadlineTask(DeadlineTask &&d) = default;

  virtual std::chrono::steady_clock::time_point get_next_deadline() {
    return std::chrono::steady_clock::now() + relative_deadline_;
  }

  virtual void work() { work_for(work_duration_); }

private:
  std::chrono::steady_clock::duration relative_deadline_;
};

class PriorityTask : public Task {
public:
  PriorityTask(std::string name, std::string pool_name,
               std::chrono::steady_clock::duration activation_interval,
               std::chrono::steady_clock::duration work_duration,
               size_t priority)
      : Task(name, pool_name, activation_interval, work_duration),
        priority_(priority) {}

  PriorityTask(PriorityTask &&p) = default;

  virtual std::chrono::steady_clock::time_point get_next_deadline() {
    return std::chrono::steady_clock::time_point::max() -
           std::chrono::microseconds(priority_);
  }

  virtual void work() { work_for(work_duration_); }

private:
  size_t priority_;
};

void multiply_matrices(const std::vector<std::vector<int>> &a,
                       const std::vector<std::vector<int>> &b,
                       std::vector<std::vector<int>> &result) {
  auto const a_rows = a.size();
  auto const a_cols = a[0].size();
  auto const b_cols = b[0].size();
  for (size_t i = 0; i < a_rows; i++) {
    for (size_t j = 0; j < b_cols; j++) {
      auto v = 0;
      for (size_t k = 0; k < a_cols; k++) {
        v += a[i][k] * b[k][j];
        result[i][j] = v;
      }
      yield_maybe();
    }
  }
}

void multiply_matrices_parallel(
				const std::string pool_name_,
				const std::chrono::steady_clock::time_point deadline,
    const std::vector<std::vector<int>> &a,
    const std::vector<std::vector<int>> &b,
    std::vector<std::vector<int>> &result) {
  hpx::threads::executors::pool_executor executor(pool_name_, deadline);
  auto const a_rows = a.size();
  auto const a_cols = a[0].size();
  auto const b_cols = b[0].size();
  std::vector<hpx::future<void>> futures(a_rows);
  for (size_t i = 0; i < a_rows; i++) {
    futures[i] = hpx::async(executor, [&, i]() {
      for (size_t j = 0; j < b_cols; j++) {
        auto v = 0;
        for (size_t k = 0; k < a_cols; k++) {
          v += a[i][k] * b[k][j];
        }
        yield_maybe();
        result[i][j] = v;
      }
    });
  }
  hpx::wait_all(futures);
}

static constexpr size_t SIZE = 100;

class SequentialMatrixTask : public Task {
public:
  SequentialMatrixTask(std::string name, std::string pool_name,
                       std::chrono::steady_clock::duration activation_interval,
                       std::chrono::steady_clock::duration relative_deadline)
      : Task(name, pool_name, activation_interval, std::chrono::seconds(0)),
        relative_deadline_(relative_deadline), a(SIZE), b(SIZE), result(SIZE) {
    for (size_t i = 0; i < a.size(); i++) {
      auto tmp = std::vector<int>(SIZE);
      std::generate(tmp.begin(), tmp.end(), std::rand);
      a[i] = tmp;
    }
    for (size_t i = 0; i < a.size(); i++) {
      auto tmp = std::vector<int>(SIZE);
      std::generate(tmp.begin(), tmp.end(), std::rand);
      b[i] = tmp;
    }
    for (size_t i = 0; i < result.size(); i++) {
      result[i] = std::vector<int>(SIZE);
    }
  }

  SequentialMatrixTask(SequentialMatrixTask &&d) = default;

  virtual std::chrono::steady_clock::time_point get_next_deadline() {
    return std::chrono::steady_clock::now() + relative_deadline_;
  }

  virtual void work() { multiply_matrices(a, b, result); }

private:
  std::chrono::steady_clock::duration relative_deadline_;
  std::vector<std::vector<int>> a;
  std::vector<std::vector<int>> b;
  std::vector<std::vector<int>> result;
};


class ParallelMatrixTask : public Task {
public:
  ParallelMatrixTask(std::string name, std::string pool_name,
                     std::chrono::steady_clock::duration activation_interval,
                     std::chrono::steady_clock::duration relative_deadline)
      : Task(name, pool_name, activation_interval, std::chrono::seconds(0)),
        relative_deadline_(relative_deadline), a(SIZE), b(SIZE), result(SIZE) {
    for (size_t i = 0; i < a.size(); i++) {
      auto tmp = std::vector<int>(SIZE);
      std::generate(tmp.begin(), tmp.end(), std::rand);
      a[i] = tmp;
    }
    for (size_t i = 0; i < a.size(); i++) {
      auto tmp = std::vector<int>(SIZE);
      std::generate(tmp.begin(), tmp.end(), std::rand);
      b[i] = tmp;
    }
    for (size_t i = 0; i < result.size(); i++) {
      result[i] = std::vector<int>(SIZE);
    }
  }

  ParallelMatrixTask(ParallelMatrixTask &&d) = default;

  virtual std::chrono::steady_clock::time_point get_next_deadline() {
    return std::chrono::steady_clock::now() + relative_deadline_;
  }

  virtual void work() {
    multiply_matrices_parallel(pool_name_, get_next_deadline(), a, b, result);
  }

private:
  std::chrono::steady_clock::duration relative_deadline_;
  std::vector<std::vector<int>> a;
  std::vector<std::vector<int>> b;
  std::vector<std::vector<int>> result;
};

std::vector<std::unique_ptr<Task>> read_priority_tasks() {
  std::ifstream in("priority_tasks.txt");
  std::vector<std::unique_ptr<Task>> v;
  std::string name, pool_name;
  std::size_t activation_interval, work_duration, priority;
  while (in >> name >> pool_name >> activation_interval >> work_duration >>
         priority) {
    v.push_back(std::unique_ptr<Task>(new PriorityTask(
        name, pool_name, std::chrono::microseconds(activation_interval - 129),
        std::chrono::microseconds(work_duration), priority)));
  }
  return v;
}

std::vector<std::unique_ptr<Task>> read_deadline_tasks() {
  std::ifstream in("deadline_tasks.txt");
  std::vector<std::unique_ptr<Task>> v;
  std::string name, pool_name;
  std::size_t activation_interval, work_duration, relative_deadline;
  while (in >> name >> pool_name >> activation_interval >> work_duration >>
         relative_deadline) {
    v.push_back(std::unique_ptr<Task>(new DeadlineTask(
        name, pool_name, std::chrono::microseconds(activation_interval - 129),
        std::chrono::microseconds(work_duration),
        std::chrono::microseconds(relative_deadline))));
  }

  return v;
}

std::vector<std::unique_ptr<Task>> read_matrix_tasks() {
  std::ifstream in("matrix_tasks.txt");
  std::vector<std::unique_ptr<Task>> v;
  std::string name, pool_name;
  std::size_t activation_interval, work_duration, relative_deadline;
  while (in >> name >> pool_name >> activation_interval >> work_duration >>
         relative_deadline) {
    if (name == "T_MATRIX_MULTIPLICATION") {
      v.push_back(std::unique_ptr<Task>(new SequentialMatrixTask(
          "T_MATRIX_MULTIPLICATION", "matrix",
          std::chrono::microseconds(activation_interval),
          std::chrono::microseconds(relative_deadline))));
      continue;
    }
    v.push_back(std::unique_ptr<Task>(new DeadlineTask(
        name, pool_name, std::chrono::microseconds(activation_interval - 129),
        std::chrono::microseconds(work_duration),
        std::chrono::microseconds(relative_deadline))));
  }
  return v;
}

void run_test(std::vector<std::unique_ptr<Task>> &tasks, std::string name) {
  for (auto &task : tasks) {
    task->start();
  }
  hpx::cout << "Started all tasks" << hpx::endl;
  hpx::this_thread::sleep_for(RUNTIME);
  std::ofstream out;
  out.open(name);
  for (auto &task : tasks) {
    task->stop();
  }
  hpx::this_thread::sleep_for(std::chrono::seconds(2));
  for (auto &task : tasks) {
    auto timings = task->get_timings();
    for (auto &timing : timings) {
      std::string name;
      switch (timing.event) {
      case TaskTimingEvent::ACTIVATION:
        name = "activation";
        break;
      case TaskTimingEvent::WORK_STARTED:
        name = "work_started";
        break;
      case TaskTimingEvent::WORK_DONE:
        name = "work_done";
        break;
      default:
        std::cerr << "Received timing data with no event" << std::endl;
        exit(1);
        break;
      }
      out << "task " << task->get_name() << " activation " << timing.activation
          << " " << name << " " << timing.time.time_since_epoch().count()
          << std::endl;
    }
  }
  out.close();
  std::cout << "Timings written to file system" << std::endl;
}

int hpx_main(boost::program_options::variables_map &vm) {
  std::size_t num_threads = hpx::get_num_worker_threads();
  /**
  auto deadline_tasks = read_deadline_tasks();
  run_test(deadline_tasks, "deadline_tasks_out.txt");
  hpx::this_thread::sleep_for(RUNTIME);
  auto priority_tasks = read_priority_tasks();
  run_test(priority_tasks, "priority_tasks_out.txt");
  hpx::this_thread::sleep_for(RUNTIME);
  **/
  auto matrix_tasks = read_matrix_tasks();
  run_test(matrix_tasks, "matrix_tasks_out.txt");
  return hpx::finalize();
}

int main(int argc, char *argv[]) {
  boost::program_options::options_description desc_cmdline("Test options");
  hpx::resource::partitioner rp(desc_cmdline, argc, argv);
  rp.create_thread_pool(
      "default",
      [](hpx::threads::policies::callback_notifier &notifier,
         std::size_t num_threads, std::size_t thread_offset,
         std::size_t pool_index, std::string const &pool_name)
          -> std::unique_ptr<hpx::threads::detail::thread_pool_base> {
        std::unique_ptr<edf_scheduler> scheduler(
            new edf_scheduler(num_threads));

        auto mode = hpx::threads::policies::scheduler_mode(
            hpx::threads::policies::scheduler_mode::delay_exit);

        std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
            new hpx::threads::detail::scheduled_thread_pool<edf_scheduler>(
                std::move(scheduler), notifier, pool_index, pool_name, mode,
                thread_offset));
        return pool;
      });

  std::map<std::string, int> custom_pools = {// {"A0", 1}, {"A1", 1}, {"A2", 1},
                                             {"matrix", 2}};

  for (auto const &entry : custom_pools) {
    rp.create_thread_pool(
        entry.first,
        [entry](hpx::threads::policies::callback_notifier &notifier,
                std::size_t num_threads, std::size_t thread_offset,
                std::size_t pool_index, std::string const &pool_name)
            -> std::unique_ptr<hpx::threads::detail::thread_pool_base> {
          std::unique_ptr<edf_scheduler> scheduler(
              new edf_scheduler(entry.second));
          auto mode = hpx::threads::policies::scheduler_mode(
              hpx::threads::policies::scheduler_mode::delay_exit);
          std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
              new hpx::threads::detail::scheduled_thread_pool<edf_scheduler>(
                  std::move(scheduler), notifier, pool_index, pool_name, mode,
                  thread_offset));
          return pool;
        });
  }

  for (const hpx::resource::numa_domain &d : rp.numa_domains()) {
    for (const hpx::resource::core &c : d.cores()) {
      for (const hpx::resource::pu &p : c.pus()) {
        for (auto const &entry : custom_pools) {
          if (custom_pools[entry.first] > 0) {
            std::cout << "Added processing unit to pool " << entry.first
                      << std::endl;
            rp.add_resource(p, entry.first);
            custom_pools[entry.first] = entry.second - 1;
            break;
          }
        }
      }
    }
  }

  return hpx::init();
}
