with open('/tmp/out.txt') as f:
    lines = f.readlines()


lines = [line.split(' ') for line in lines]
lines = [line for line in lines if 'task_details' not in line]
lines = [[line[1], int(line[3]), line[4], int(line[5].strip()) // 1000] for line in lines]
tasks = {}
import itertools
for k, g in itertools.groupby(lines, lambda k: k[0]):
    g = [x[1:] for x in g]
    g.sort()
    activations = {}
    for activation_id, group in itertools.groupby(g, lambda k: k[0]):
        group = list(group)
        activations[activation_id] = {x[1]: x[2] for x in group}
        if not all(k in activations[activation_id]
                   for k in ('activation', 'work_started', 'work_done')):
            del activations[activation_id]
    tasks[k] = {'activations': activations}

with open("deadline_tasks.txt") as f:
    for line in f.readlines():
        line = line.split(' ')
        task = line[0]
        tasks[task]['core'] = line[1]
        tasks[task]['period'] = int(line[2])
        tasks[task]['work_time'] = int(line[3])
        tasks[task]['deadline'] = int(line[4])
        print("Loaded task {} core {} period {} work_time {} deadline {}".format(
            task, tasks[task]['core'], tasks[task]['period'], tasks[task]['work_time'],
            tasks[task]['deadline'],
        ))

import statistics
from tabulate import tabulate


for task_id, task in tasks.items():
    activations = [act['activation'] for _, act in task['activations'].items()]
    inter_activation_times = [two - one for one, two in zip(activations,
        activations[1:])]
    task['inter_activation_times'] = inter_activation_times
    task['late_activations'] = 0
    for activation, data in task['activations'].items():
        data['startup_time'] = data['work_started'] - data['activation']
        data['response_time'] = data['work_done'] - data['activation']
        data['work_time'] = data['work_done'] - data['work_started']
        data['lateness'] = data['work_done'] - (data['activation'] +
            task['deadline'])
        if data['lateness'] > 0:
            task['late_activations'] += 1

def print_late_activations():
    data = []
    for task_id, task in tasks.items():
        latenesses = [a['lateness'] for a in task['activations'].values()]
        data.append((
            task_id,
            task['late_activations'],
            len(task['activations']),
            int(statistics.mean(latenesses)),
            min(latenesses),
            max(latenesses),
        ))
    print(tabulate(data, headers=[
        'Task', 'Late activations', 'Total activations',
        'Mean lateness [us]',
        'Min lateness [us]',
        'Max lateness [us]',
    ]))


def print_inter_arrival_times():
    data = []
    for task_id, task in tasks.items():
        data.append((
            task_id,
            int(statistics.mean(task['inter_activation_times'])),
            min(task['inter_activation_times']),
            max(task['inter_activation_times']),
            task['period'],
        ))
    print(tabulate(data, headers=[
        'Task', 'Mean inter-activation time [us]',
        'Min inter-activation time [us]',
        'Max inter-activation time [us]',
        'Period'
    ]))


def print_work_times():
    for task_id, task in tasks.items():
        work_times = [act['work_time'] for _, act in
                task['activations'].items()]
        print(
            "Task {} work times: mean({}) min({}) max({})".format(
                task_id,
                int(statistics.mean(work_times)),
                min(work_times),
                max(work_times),
            )
        )


def print_startup_times():
    for task_id, task in tasks.items():
        startup_times = [act['startup_time'] for _, act in
                task['activations'].items()]
        print(
            "Task {} startup times: mean({}) min({}) max({})".format(
                task_id,
                int(statistics.mean(startup_times)),
                min(startup_times),
                max(startup_times),
            )
        )


print_inter_arrival_times()
print_late_activations()
print_work_times()
print_startup_times()

import pandas
from collections import OrderedDict
data = {}
for task_id, task in tasks.items():
    startup_times = [act['startup_time'] for _, act in
        task['activations'].items()]
    work_times = [act['work_time'] for _, act in
        task['activations'].items()]
    latenesses = [a['lateness'] for a in task['activations'].values()]
    data[task_id] = {
        'Period': task['period'],
        'Activations': len(task['activations']),
        'Deadline misses': task['late_activations'],
        'Lateness': int(statistics.mean(latenesses)),
        'Work time': task['work_time'],
        'Deadline': task['deadline'],
        'Startup time': int(statistics.mean(startup_times)),
        'Work time': int(statistics.mean(work_times)),
        'Inter-arrival time': int(statistics.mean(task['inter_activation_times'])),
    }
df = pandas.DataFrame(data)
print(df)
