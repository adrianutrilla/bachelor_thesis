# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/autrilla/Projects/bachelor_thesis/tasks/tasks.cpp" "/Users/autrilla/Projects/bachelor_thesis/tasks/CMakeFiles/tasks_exe.dir/tasks.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HPX_APPLICATION_EXPORTS"
  "HPX_APPLICATION_NAME=tasks_exe"
  "HPX_APPLICATION_STRING=\"tasks_exe\""
  "HPX_PREFIX=\"/Users/autrilla/Projects/hpx-clang\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/autrilla/Projects/hpx-local"
  "/Users/autrilla/Projects/hpx-clang"
  "/Users/autrilla/Projects/hpx-local/examples"
  "/Users/autrilla/Projects/hpx-local/tests"
  "/usr/local/include"
  "/usr/local/Cellar/hwloc/1.11.8/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
