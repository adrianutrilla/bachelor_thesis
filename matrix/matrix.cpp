#include <hpx/hpx.hpp>
#include <hpx/hpx_init.hpp>

#include <hpx/runtime/resource/partitioner.hpp>
#include <hpx/runtime/threads/detail/scheduled_thread_pool_impl.hpp>
#include <hpx/runtime/threads/executors/default_edf_executor.hpp>
#include <hpx/runtime/threads/policies/edf_scheduler.hpp>

#include <hpx/include/iostreams.hpp>
#include <hpx/include/runtime.hpp>

#include <chrono>
#include <vector>

using edf_scheduler = hpx::threads::policies::edf_scheduler<>;

void yield_maybe_synchronized() {
  hpx::threads::thread_data *next_thread = nullptr;
  auto self = hpx::this_thread::get_id().native_handle().get();
  auto scheduler = dynamic_cast<edf_scheduler *>(self->get_scheduler_base());
  scheduler->peek_next_thread(next_thread);
  if (next_thread != NULL && self != NULL) {
    if (next_thread->get_deadline() < self->get_deadline()) {
      hpx::this_thread::yield();
    }
  }
}

void yield_maybe() {
  auto self = hpx::this_thread::get_id().native_handle().get();
  auto scheduler = dynamic_cast<edf_scheduler *>(self->get_scheduler_base());
  auto next_deadline = scheduler->peek_next_deadline();
  if (self != NULL) {
    if (next_deadline < self->get_deadline()) {
      hpx::this_thread::yield();
    }
  }
}

void no_yield() {}

void multiply_matrices(const std::vector<std::vector<int>> &a,
                       const std::vector<std::vector<int>> &b,
                       std::vector<std::vector<int>> &result) {
  auto const a_rows = a.size();
  auto const a_cols = a[0].size();
  auto const b_cols = b[0].size();
  for (size_t i = 0; i < a_rows; i++) {
    for (size_t j = 0; j < b_cols; j++) {
      auto v = 0;
      for (size_t k = 0; k < a_cols; k++) {
        v += a[i][k] * b[k][j];
        result[i][j] = v;
      }
    }
  }
}

void multiply_matrices_parallel(void (*yield)(void),
                                const std::vector<std::vector<int>> &a,
                                const std::vector<std::vector<int>> &b,
                                std::vector<std::vector<int>> &result) {
  hpx::threads::executors::default_edf_executor executor(
      std::chrono::steady_clock::now());
  auto const a_rows = a.size();
  auto const a_cols = a[0].size();
  auto const b_cols = b[0].size();
  std::vector<hpx::future<void>> futures(a_rows);
  for (size_t i = 0; i < a_rows; i++) {
    futures[i] = hpx::async(executor, [&, i]() {
      for (size_t j = 0; j < b_cols; j++) {
        auto v = 0;
        for (size_t k = 0; k < a_cols; k++) {
          v += a[i][k] * b[k][j];
        }
        yield();
        result[i][j] = v;
      }
    });
  }
  hpx::wait_all(futures);
}

template <typename F> void matrix_test(F f, std::string name) {
  const auto iters = 10000;
  std::vector<size_t> measurements(iters);
  for (auto i = 0; i < iters; i++) {
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    f();
    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    measurements[i] = duration;
    std::cerr << name << " " << i << "/" << iters << "\r";
  }
  std::ofstream out;
  std::string filename = name + ".txt";
  out.open(filename);
  for (auto x : measurements) {
    out << x << std::endl;
  }
  out.close();
  std::cout << name << " done" << std::endl;
}

int hpx_main(boost::program_options::variables_map &vm) {
  const size_t size = 300;
  std::vector<std::vector<int>> a(size);
  for (size_t i = 0; i < a.size(); i++) {
    auto tmp = std::vector<int>(size);
    std::generate(tmp.begin(), tmp.end(), std::rand);
    a[i] = tmp;
  }
  std::vector<std::vector<int>> b(size);
  for (size_t i = 0; i < a.size(); i++) {
    auto tmp = std::vector<int>(size);
    std::generate(tmp.begin(), tmp.end(), std::rand);
    b[i] = tmp;
  }
  std::vector<std::vector<int>> result(size);
  for (size_t i = 0; i < result.size(); i++) {
    result[i] = std::vector<int>(size);
  }
  std::cout << "Random matrices generated" << std::endl;
  matrix_test([&]() { multiply_matrices(a, b, result); }, "sequential");
  matrix_test(
      [&]() {
        multiply_matrices_parallel(no_yield, a, b, result);
      },
      "parallel");
  matrix_test(
      [&]() {
        multiply_matrices_parallel(hpx::this_thread::yield, a, b, result);
      },
      "parallel_yield");
  matrix_test(
      [&]() {
        multiply_matrices_parallel(yield_maybe, a, b, result);
      },
      "parallel_yield_maybe");
  matrix_test(
      [&]() {
        multiply_matrices_parallel(yield_maybe_synchronized, a, b, result);
      },
      "parallel_yield_maybe_synchronized");
  return hpx::finalize();
}

int main(int argc, char *argv[]) {
  boost::program_options::options_description desc_cmdline("Test options");
  hpx::resource::partitioner rp(desc_cmdline, argc, argv);
  rp.create_thread_pool(
      "default",
      [](hpx::threads::policies::callback_notifier &notifier,
         std::size_t num_threads, std::size_t thread_offset,
         std::size_t pool_index, std::string const &pool_name)
          -> std::unique_ptr<hpx::threads::detail::thread_pool_base> {
        std::unique_ptr<edf_scheduler> scheduler(
            new edf_scheduler(num_threads));

        auto mode = hpx::threads::policies::scheduler_mode(
            hpx::threads::policies::scheduler_mode::do_background_work |
            hpx::threads::policies::scheduler_mode::delay_exit);

        std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
            new hpx::threads::detail::scheduled_thread_pool<edf_scheduler>(
                std::move(scheduler), notifier, pool_index, pool_name, mode,
                thread_offset));
        return pool;
      });
  return hpx::init();
}
