#include <hpx/hpx.hpp>
#include <hpx/hpx_init.hpp>

#include <hpx/runtime/resource/partitioner.hpp>
#include <hpx/runtime/threads/detail/scheduled_thread_pool_impl.hpp>
#include <hpx/runtime/threads/policies/edf_scheduler.hpp>

#include <hpx/include/iostreams.hpp>
#include <hpx/include/runtime.hpp>

#include <chrono>
#include <vector>

using edf_scheduler = hpx::threads::policies::edf_scheduler<>;

int hpx_main(boost::program_options::variables_map &vm) {
  std::size_t num_threads = hpx::get_num_worker_threads();
  const auto iters = 10000;
  std::vector<std::size_t> measurements(iters);
  for (auto i = 0; i < iters; i++) {
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    hpx::async([]() {}).get();
    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    measurements[i] = duration;
  }
  for (auto x : measurements) {
    hpx::cout << x << hpx::endl;
  }
  return hpx::finalize();
}

int main(int argc, char *argv[]) {
  boost::program_options::options_description desc_cmdline("Test options");
  hpx::resource::partitioner rp(desc_cmdline, argc, argv);
  rp.create_thread_pool(
      "default",
      [](hpx::threads::policies::callback_notifier &notifier,
         std::size_t num_threads, std::size_t thread_offset,
         std::size_t pool_index, std::string const &pool_name)
          -> std::unique_ptr<hpx::threads::detail::thread_pool_base> {
        std::unique_ptr<edf_scheduler> scheduler(
            new edf_scheduler(num_threads));

        auto mode = hpx::threads::policies::scheduler_mode(
            hpx::threads::policies::scheduler_mode::do_background_work |
            hpx::threads::policies::scheduler_mode::delay_exit);

        std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
            new hpx::threads::detail::scheduled_thread_pool<edf_scheduler>(
                std::move(scheduler), notifier, pool_index, pool_name, mode,
                thread_offset));
        return pool;
      });
  return hpx::init();
}
