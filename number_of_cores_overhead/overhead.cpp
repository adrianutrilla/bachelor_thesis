#include <hpx/hpx.hpp>
#include <hpx/hpx_init.hpp>

#include <hpx/runtime/resource/partitioner.hpp>
#include <hpx/runtime/threads/detail/scheduled_thread_pool_impl.hpp>
#include <hpx/runtime/threads/policies/edf_scheduler.hpp>

#include <hpx/include/iostreams.hpp>
#include <hpx/include/runtime.hpp>

#include <chrono>
#include <vector>

using edf_scheduler_mutex = hpx::threads::policies::edf_scheduler<std::mutex>;
using edf_scheduler_spinlock =
    hpx::threads::policies::edf_scheduler<hpx::util::spinlock>;

void sleep_for(std::chrono::steady_clock::duration d) {
  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  auto wait_until = t1 + d;
  while (std::chrono::steady_clock::now() < wait_until)
    ;
}

void run_empty_tasks(std::vector<hpx::future<void>> &futures,
                     std::chrono::steady_clock::duration d) {
  for (auto i = 0; i < futures.size(); i++) {
    futures[i] = hpx::async([&]() { sleep_for(d); });
  }
  hpx::wait_all(futures);
}

int hpx_main(boost::program_options::variables_map &vm) {
  using namespace std::chrono_literals;
  std::size_t num_threads = hpx::get_num_worker_threads();
  std::vector<hpx::future<void>> futures(num_threads);
  const auto iters = 10000;
  std::vector<std::size_t> measurements(iters);
  auto sleep_duration = 100us;
  for (auto i = 0; i < iters; i++) {
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    run_empty_tasks(futures, sleep_duration);
    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
    auto overhead =
        (std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1) -
         sleep_duration)
            .count();
    measurements[i] = overhead;
  }
  std::ofstream out;
  std::string test_type = vm["locktype"].as<std::string>();
  out.open(test_type + std::string("-") + std::to_string(num_threads) +
           std::string(".txt"));
  for (auto x : measurements) {
    out << x << std::endl;
  }
  out.close();
  return hpx::finalize();
}

int main(int argc, char *argv[]) {
  boost::program_options::options_description desc_cmdline("Options");
  desc_cmdline.add_options()(
      "locktype",
      boost::program_options::value<std::string>()->default_value("mutex"),
      "The lock type to use");
  boost::program_options::variables_map vm;
  boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv)
          .allow_unregistered()
          .options(desc_cmdline)
          .run(),
      vm);
  hpx::resource::partitioner rp(desc_cmdline, argc, argv);
  rp.create_thread_pool(
      "default",
      [&](hpx::threads::policies::callback_notifier &notifier,
          std::size_t num_threads, std::size_t thread_offset,
          std::size_t pool_index, std::string const &pool_name)
          -> std::unique_ptr<hpx::threads::detail::thread_pool_base> {
        auto mode = hpx::threads::policies::scheduler_mode(
            hpx::threads::policies::scheduler_mode::do_background_work |
            hpx::threads::policies::scheduler_mode::delay_exit);

        if (vm["locktype"].as<std::string>() == "mutex") {
          std::unique_ptr<edf_scheduler_mutex> scheduler(
              new edf_scheduler_mutex(num_threads));
          std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
              new hpx::threads::detail::scheduled_thread_pool<
                  edf_scheduler_mutex>(std::move(scheduler), notifier,
                                       pool_index, pool_name, mode,
                                       thread_offset));
          return pool;
        } else {
          std::unique_ptr<edf_scheduler_spinlock> scheduler(
              new edf_scheduler_spinlock(num_threads));
          std::unique_ptr<hpx::threads::detail::thread_pool_base> pool(
              new hpx::threads::detail::scheduled_thread_pool<
                  edf_scheduler_spinlock>(std::move(scheduler), notifier,
                                          pool_index, pool_name, mode,
                                          thread_offset));
          return pool;
        }
      });
  return hpx::init();
}
