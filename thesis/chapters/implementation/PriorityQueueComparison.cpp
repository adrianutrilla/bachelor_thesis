typedef thread_data thread_description;

// The comparison function for the scheduler's task container
static bool work_items_ordering(thread_description *left,
                                thread_description *right) {
  return left->get_deadline() > right->get_deadline();
};

// Type definition for the scheduler's task container
typedef std::priority_queue<
    thread_description *, std::vector<thread_description *>,
    decltype(&work_items_ordering)>
    work_items_type;

// Initialization of the scheduler's task container passes in
// the comparison function as a constructor argument
work_items_type work_items_(&work_items_ordering);
