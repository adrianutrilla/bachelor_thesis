using namespace std::literals::chrono_literals;
auto deadline = std::chrono::steady_clock::now() + 10ms;
// An executor that executes work on the default thread pool
// with the given deadline
hpx::threads::executors::default_edf_executor
    default_exec(deadline);
hpx::async(default_exec, []() {
  // Do work
});
// An executor that executes work on the thread pool named
// "pool1" with the given deadline
hpx::threads::executors::pool_executor pool_exec("pool1",
                                                 deadline);
hpx::async(pool_exec, []() {
  // Do work
});
