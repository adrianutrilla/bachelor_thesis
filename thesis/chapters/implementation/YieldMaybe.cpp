void yield_maybe() {
  auto self =
      hpx::this_thread::get_id().native_handle().get();
  auto scheduler = dynamic_cast<edf_scheduler *>(
      self->get_scheduler_base());
  auto next_deadline = scheduler->peek_next_deadline();
  if (next_deadline < self->get_deadline()) {
    hpx::this_thread::yield();
  }
}
