thread_data *next_thrd = nullptr;
while (true) {
  bool has_work =
      scheduler.SchedulingPolicy::get_next_thread(next_thrd);
  if (has_work) {
    thread_state state = next_thrd->get_state();
    if (state == pending) {
      mark_thread_active(next_thrd);
    }
    // Run thread until it terminates or yields
    scheduler.SchedulingPolicy::schedule_thread(next_thrd);
    if (state == depleted || state == terminated) {
      // If the thread is done, remove it from the scheduler
      scheduler.SchedulingPolicy::destroy_thread(thrd);
    }
  } else {
    do_background_work_or_terminate();
  }
}
