const auto iters = 10000;
std::vector<std::size_t> measurements(iters);
for (auto i = 0; i < iters; i++) {
  std::chrono::steady_clock::time_point t1 =
      std::chrono::steady_clock::now();
  hpx::async([]() {}).get();
  std::chrono::steady_clock::time_point t2 =
      std::chrono::steady_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(
          t2 - t1)
          .count();
  measurements[i] = duration;
}
