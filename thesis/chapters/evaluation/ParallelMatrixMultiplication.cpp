void multiply_matrices_parallel(
    void (*yield)(void),
    const std::vector<std::vector<int>> &a,
    const std::vector<std::vector<int>> &b,
    std::vector<std::vector<int>> &result) {
  hpx::threads::executors::default_edf_executor executor(
      std::chrono::steady_clock::now());
  auto const a_rows = a.size();
  auto const a_cols = a[0].size();
  auto const b_cols = b[0].size();
  std::vector<hpx::future<void>> futures(a_rows);
  for (size_t i = 0; i < a_rows; i++) {
    futures[i] = hpx::async(executor, [&, i]() {
      for (size_t j = 0; j < b_cols; j++) {
        auto v = 0;
        for (size_t k = 0; k < a_cols; k++) {
          v += a[i][k] * b[k][j];
        }
        yield();
        result[i][j] = v;
      }
    });
  }
  hpx::wait_all(futures);
}
