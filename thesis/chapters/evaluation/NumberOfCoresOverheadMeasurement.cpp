void sleep_for(std::chrono::steady_clock::duration d) {
  std::chrono::steady_clock::time_point t1 =
      std::chrono::steady_clock::now();
  auto wait_until = t1 + d;
  while (std::chrono::steady_clock::now() < wait_until)
    ;
}

void run_empty_tasks(std::vector<hpx::future<void>> &futures,
                     std::chrono::steady_clock::duration d) {
  for (auto i = 0; i < futures.size(); i++) {
    futures[i] = hpx::async([&]() { sleep_for(d); });
  }
  hpx::wait_all(futures);
}

// ...
std::vector<hpx::future<void>> futures(num_threads);
auto sleep_duration = 100us;
std::chrono::steady_clock::time_point t1 =
    std::chrono::steady_clock::now();
run_empty_tasks(futures, sleep_duration);
std::chrono::steady_clock::time_point t2 =
    std::chrono::steady_clock::now();
auto overhead =
    (std::chrono::duration_cast<std::chrono::microseconds>(
         t2 - t1) -
     sleep_duration)
        .count();
measurements[i] = overhead;
// ...
