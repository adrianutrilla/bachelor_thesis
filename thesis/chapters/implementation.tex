\chapter{Implementation of an \acrshort{edf} scheduler for HPX}

HPX allows users to customize the scheduling policy used to schedule
HPX threads, and even comes with seven different scheduling policies
built in\cite{ste||ar_group_hpx_2018-1}:

\begin{itemize}
\item{Priority Local Scheduling Policy (default policy)}
\item{Static Priority Scheduling Policy}
\item{Local Scheduling Policy}
\item{Static Scheduling Policy}
\item{Priority ABP Scheduling Policy}
\item{Hierarchy Scheduling Policy}
\item{Periodic Priority Scheduling Policy}
\end{itemize}

However, none of these scheduling policies takes real-time system
requirements into consideration, so a scheduling policy that covers
those requirements had to be implemented. For this thesis, a Global
\acrshort{edf} scheduler was implemented. Global \acrshort{edf} is a
version of the Earliest Deadline First scheduling algorithm that uses
a single queue for all the processing units the scheduler
manages. \acrshort{edf} guarantees optimality for workloads with
utilization lower than 100\%\cite{liu_scheduling_1973}.

\section{Cooperative multitasking}

Global \acrshort{edf} (and \acrshort{edf}) rely on preemption in order
to ensure optimality, that is, that all deadlines are met. However,
HPX, being a user-space program, does not have the capability of
preempting HPX threads once they have started running. For this
reason, the implementation of the \acrshort{edf} scheduler for this
thesis relies on tasks being cooperative. In cooperative multitasking
systems, the scheduler never forcefully context-switches a running
task. Tasks themselves decide when to yield control back to the
scheduler. Tasks are free to do this periodically, when they're
waiting for other data, or they can simply decide to never yield
control back to the scheduler. However, fairness might suffer in the
case where tasks never yield to the scheduler, as they will simply
continue running until they terminate.

Cooperative multitasking is very popular, especially in the context of
asynchronous I/O. Programming languages such as
Go\cite{deshpande_analysis_2012} and
Python\cite{python_software_foundation_18.5.3._2018} implement
cooperative multitasking, with goroutines and generator-based
coroutines respectively. These languages implement cooperative
multitasking so that when a task is waiting on I/O, it can yield to
the scheduler and other tasks can make progress.

\section{The HPX scheduling loop}

As mentioned earlier in this thesis, HPX runs an \acrshort{os} thread
for each of the processing units it's assigned. All \acrshort{os}
threads run by HPX run the HPX scheduling loop. The scheduling loop
constanly asks the scheduling policy what thread it should run next,
and then schedules that thread. Listing
\ref{listing:implementation:scheduling-loop} shows a simplified
overview of how the HPX scheduling loop works.

\begin{listing}[H]
\cppfile{./chapters/implementation/SchedulingLoop.cpp}
  \caption{A simplified representation of the HPX scheduling loop}
  \label{listing:implementation:scheduling-loop}
\end{listing}

\section{HPX thread representation}

HPX stores information related to HPX threads in a data structure
called
\mintinline{C++}{thread_data}\cite{ste||ar_group_thread_data.hpp_2018}. This
data structure contains information such as the HPX thread's priority,
its stack, the thread pool it is running on and the thread's state. In
order to implement \acrshort{edf} scheduling, the deadline for a given
task has to be stored somewhere. Since a deadline is similar in nature
to a priority, the best place to store it is in the
\mintinline{C++}{thread_data} data structure. The deadline is stored
using the C++ standard library type
\mintinline{C++}{std::chrono::steady_clock::time_point}. This type
represents a timestamp from the C++ standard library steady clock.

\section{Priority queues}

A priority queue is an abstract data type that is similar to a
traditional queue, but each of its elements has a priority assigned to
it\cite{cormen_introduction_2009}. Instead of items being served in a
\acrshort{fifo} (first in first out) fashion, items are sorted based
on their priority. Priority queues can be implemented on top of other
data structures such as arrays, linked lists or heaps.

Priority queues are typically implemented on top of heaps in order to
make them more efficient. This makes insertion into the queue
$O(\log{}n)$ and retrieving the item from the top of the queue
$O(1)$. On the other hand, priority queues implemented on top of a
sorted array will have equal $O(1)$ time complexity for retrieving the
item from the top of the queue, but inserting items into the queue
will be $O(n)$ as items have to be moved to make space for the newly
inserted item. Implementing a priority queue on top of an unsorted
array provides a different compromise, with $O(n)$ retrieval but
$O(1)$ insertion. The C++ Standard Library implementation of priority
queues, \mintinline{C++}{std::priority_queue} is in fact implemented
on top of a heap. In our particular case, a priority queue backed by a
heap is desirable, as our most common operation is retrieving the
topmost item.

In our case, we want the queue to be sorted such that the tasks with
the earliest deadline are at the top. C++'s
\mintinline{C++}{std::priority_queue} allows customizing the
comparison function in order to achieve a custom ordering. Listing
\ref{listing:implementation:priority-queue-comparison} shows the
comparison function the implemented \acrshort{edf} scheduler uses. The
priority queue contains pointers to \mintinline{C++}{thread_data}
objects, which contain all the data required to run an HPX thread. The
comparison function uses the deadline stored in the
\mintinline{C++}{thread_data} objects to order the items in the
queue. The \acrshort{edf} scheduler uses a
\mintinline{C++}{std::vector} as the underlying container for the
priority queue.

\begin{listing}[H]
\cppfile{./chapters/implementation/PriorityQueueComparison.cpp}
  \caption{The priority queue backing the \acrshort{edf} scheduling
policy}
  \label{listing:implementation:priority-queue-comparison}
\end{listing}

\section{Scheduling policy}

A new scheduling policy had to be created in order to schedule threads
ordered by their deadline. For this, the Priority Local Scheduling
Policy built-in to HPX was copied and modified. The Priority Local
Scheduling Policy contains two queues for each processing unit it
manages: a normal priority queue and a high priority queue. Both of
these queues are simple \acrshort{fifo} queues. They are implemented
in the
\mintinline{C++}{thread_queue}\cite{ste||ar_group_thread_queue.hpp_2018}
class.

The first step taken was removing all unnecessary instrumentation,
NUMA handling and work-stealing capabilities, as they are not needed
for a proof-of concept scheduler and removing them simplifies
development later on.

The next step towards adapting the copy of the Priority Local
Scheduling Policy towards an \acrshort{edf} scheduling policy was
migrating to using a single queue. Since Global \acrshort{edf} is to
be implemented, the scheduler needs to only have a single queue for
all processing units, instead of two queues per processing unit. This
requires additional synchronization to make sure no more than one
thread concurrently accesses this queue, as
\mintinline{C++}{std::priority_queue} is not thread-safe.

The \mintinline{C++}{thread_queue} class was similarly copied as
\mintinline{C++}{edf_queue} and then modified, stripping all
unnecessary code and modifying the backing store from a simple
\acrshort{fifo} queue to the aforementioned priority queue. Lastly,
the single \mintinline{C++}{thread_queue} used in the scheduling
policy was replaced with the new \mintinline{C++}{edf_queue}.

\section{Executors}

An \acrshort{api} has to be provided such that when creating a new
task, it is possible to indicate its deadline so that the scheduler
can handle it appropriately. We do this through the concept of
executors. An executor is an object responsible for creating execution
agents on which work is performed, abstracting the mechanisms for
launching work\cite{ste||ar_group_executors_2018}. Executors are
responsible for registering HPX threads with the scheduler by creating
\mintinline{C++}{thread_data} objects and passing them to the
scheduler. Additionally, executors permit changing the options of the
HPX threads they launch. HPX algorithm implementations optionally
accept an executor as their first parameter, so it is possible to
customize the options of the HPX threads launched by the algorithm. As
an example, it's possible to create a custom executor that makes all
HPX threads it runs high priority. Similarly, we can create a custom
executor that takes a deadline and registers all HPX threads with the
scheduler with that deadline information. For this thesis, two custom
executor types were implemented, the
\mintinline{C++}{default_edf_executor} and the \acrshort{edf}
\mintinline{C++}{pool_executor}. Both take a deadline as an argument,
and the latter additionally takes a thread pool name on which the work
will be run. Listing \ref{listing:implementation:executors} shows
example usage of both executors.

\begin{listing}[H] \cppfile{./chapters/implementation/Executors.cpp}
  \caption{Example usage of the implemented executors}
  \label{listing:implementation:executors}
\end{listing}

\section{Yielding optimizations}

Because HPX uses a cooperative multitasking model, tasks are expected
to yield to the scheduler. However, yielding frequently to the
scheduler can have a very big impact on performance due to unnecessary
context switches. To prevent this, an optimization was made by
creating a new function, \mintinline{C++}{yield_maybe}, that yields to
the scheduler only if it knows for sure that there are tasks with a
lower deadline waiting to run. The \acrshort{edf} scheduling policy
exposes the next earliest deadline for all its tasks, and the
\mintinline{C++}{yield_maybe} function compares this deadline with the
running HPX thread's deadline. The function yields to the scheduler
only if the running HPX thread's deadline is later than the scheduling
policy's next deadline. This function allows having many yield points
with little impact to performance. Listing
\ref{listing:implementation:yieldmaybe} shows the source code of this
function.

\begin{listing}[H] \cppfile{./chapters/implementation/YieldMaybe.cpp}
  \caption{The yield\_maybe function}
  \label{listing:implementation:yieldmaybe}
\end{listing}


%%% Local Variables: %%% mode: latex %%% TeX-master: "../thesis.tex"
%%% TeX-engine: xetex %%% End:
