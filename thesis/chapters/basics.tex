\chapter{Basics}

\section{Real-time systems}

A real-time system is a system in which the correctness of the
system's behaviors depends on both the logical results of the
computations and also on the physical time at which the results are
available\cite{khemka_multiprocessor_1992}. Real-time systems are
often a part of an embedded or cyber-physical system, and their time
contraints often come from interactions with the physical
world. Examples of real-time systems include car engine control,
flight control systems, pacemakers, and video game consoles.

The results of a real-time system must be available before a given
deadline, otherwise the usefulness of the results is
diminished. Real-time systems can be classified according to the
usefulness of the results after the
deadline\cite{khemka_multiprocessor_1992}:

\begin{itemize}
\item{Hard real-time: missing a deadline can be catastrophic}
\item{Firm real-time: the result has no value after the deadline}
\item{Soft real-time: the result still has some value after the
deadline}
\end{itemize}


Real-time systems are often confused with high-performance systems,
but they have very different requirements. High-performance systems
typically focus on maximizing throughput, but this is not the goal of
real-time systems. The main goal for a real-time system is for it to
be able to return results before their respective
deadlines\cite{stankovic_misconceptions_1988}.

Real-time systems are often multi-tasking, which means that they are
required to run multiple tasks concurrently. In such systems, tasks
are created either in response to external events (e.g. an interrupt
triggered by a sensor), or in response to time (e.g. a task that needs
to run evey 100ms). Having multiple tasks requires a scheduler to
decide which task should run at a given point in time. The following
scheduling policies are specially common:

\begin{itemize}
\item{Static priority scheduling\cite{liu_scheduling_1973}: tasks are
given a static priority at compile time. When a scheduling decision
has to be made, the task with the highest priority is picked for
execution.}
\item{Earliest deadline first\cite{liu_scheduling_1973}: tasks are
given a relative deadline from their activation point by which they
have to complete. When a scheduling decision has to be made, the task
with the earliest deadline is picked for execution.}
\item{Rate-monotonic scheduling\cite{lehoczky_rate_1989}: tasks are
given a static priority based on their period. The tasks with the
shortest periods get the highest priority. At runtime, the scheduler
selects the task with the highest priority for execution.}
\end{itemize}

  
\section{Futures}

A \texttt{future}\cite{baker_incremental_1977} is a
programming-language-independent abstraction in which an object is a
proxy for another object which might not be ready yet. It represents
the result of an operation that will be completed at some time in the
future. \texttt{future}s have an \acrshort{api} that allows at least
querying whether the \texttt{future} has completed and getting the result of
the \texttt{future} once it has completed.

\texttt{future}s can be useful in many situations where asynchronous
operations are desirable. For example, they are a useful wrapper
around I/O operations. These operations typically take many
\acrshort{cpu} cycles to complete, during which the \acrshort{cpu}
would have to sit idle, or the operating system would have to schedule
another process while waiting for the I/O operation to complete. A
function that reads a file from disk could return a \texttt{future} that
completes once the file has been read, allowing the caller to do other
work in the meantime.

\begin{listing}[H]
\cppfile{./chapters/basics/FutureSimpleExample.cpp}
  \caption{Reading a file asynchronously using a future in C++}
  \label{listing:basics:future-simple-example}
\end{listing}

\texttt{future}s are also useful for taking advantage of multi-core
systems\cite{swaine_back_2010}. Expensive computations can be wrapped
in a \texttt{future} and executed in another thread. In Listing
\ref{listing:basics:future-multiple-cores} three expensive
computations are started simultaneously by using the
\mintinline{C++}{async} function. The runtime can schedule each of the
\texttt{future}s on a different \acrshort{cpu} core so they can all
make progress in parallel. Then, the call to
\mintinline{C++}{f1.get()} will block until \mintinline{C++}{f1} is
completed, and the same will happen for the respective calls for
\mintinline{C++}{f2} and \mintinline{C++}{f3}.

\begin{listing}[H]
\cppfile{./chapters/basics/FutureMultipleCores.cpp}
  \caption{Utilizing multiple \acrshort{cpu} cores with futures in
C++}
  \label{listing:basics:future-multiple-cores}
\end{listing}

Implementations of \texttt{future}s often feature \acrshort{api}s that
allow chaining of \texttt{futures} for a more ergonomic programming
experience. A chained \texttt{future} is called a
\texttt{continuation}. \texttt{Continuations} are also executed
asynchronously, just like normal futures.

\begin{listing}[H]
\cppfile{./chapters/basics/FutureChainingExample.cpp}
  \caption{Chaining of a future in C++}
  \label{listing:basics:future-chaining}
\end{listing}

Another useful operation is waiting for more than one \texttt{future} to be
ready. When paired with \texttt{continuations}, waiting for more than
one \texttt{future} makes it possible to model asynchronous operations as a
directed graph. When using a statically and strongly typed language,
this allows representing task dependencies directly in the type
system. Additionally, this makes abstraction easy because a complex
asynchronous operation with multiple dependencies can be represented
as a simple combination of continuations and waits on multiple
\texttt{futures}. Listing \ref{listing:basics:future-task-graph} shows
one such example with \mintinline{C++}{sum_future} depending on
\mintinline{C++}{mult1} and \mintinline{C++}{mult2}.

\begin{listing}[h]
\cppfile{./chapters/basics/FutureTaskGraph.cpp}
  \caption{A future with multiple dependencies in C++}
  \label{listing:basics:future-task-graph}
\end{listing}


\section{C++}

C++ is a general-purpose programming language with object-oriented,
imperative and generic programming capabilities. In recent versions of
the language, facilities for parallelism and concurrency have been
introduced in the Parallelism Technical Specification and the
Concurrency Technical Specification, respectively.

C++11 introduced for the first time the concept of \texttt{future}s
into the standard library as \mintinline{C++}{std::future} and
\mintinline{C++}{std::shared_future}. At this point, however, features
such as \texttt{continuations} and waiting on multiple \texttt{futures} were
not part of the standard library. The Concurrency \acrshort{ts} plans
to extend the standard library with extensions that enable wait-free
composition of asynchronous operations\cite{iso_technical_2015}.  The
primary additions are support for continuations through
\mintinline{C++}{std::future::then} and support for creating a
\mintinline{C++}{std::future} object that becomes ready when all
elements in a set of \mintinline{C++}{future} and
\mintinline{C++}{shared_future} objects become ready with
\mintinline{C++}{when_all}. The Concurrency \acrshort{ts} also
introduces concepts related to thread coordination, which are useful
for synchronization between \mintinline{C++}{std::future} objects. The
concepts introduced are latches and barriers. A latch allows one or
more threads to block until an operation is completed, and can only be
used once. A barrier allows a set of participating threads to block
until an operation is completed, and is reusable.

The Parallelism \acrshort{ts}\cite{iso_technical_2015-1} on the other
hand introduces parallelized algorithms into the
\mintinline{C++}{std::algorithm} namespace. The Parallelism
\acrshort{ts} was accepted into the C++17 Standard. The Parallelism
\acrshort{ts} introduces parallelized algorithms through the concept
of \texttt{Execution Policies}, which are a representation of how an
algorithm is allowed to execute. Three different policies where added
to the standard:

\begin{itemize}
\item{seq: the algorithm is executed sequentially without any
parallelism.}
\item{par: permits the implementation to execute the algorithm on
multiple threads in parallel.}
\item{par\_vec: permits the implementation to both execute the
algorithm on multiple threads in parallel and also to vectorize the
algorithm.}
\end{itemize}

The programmer can then pass the policy to the standard library
algorithms that support it. An example is shown in Listing
\ref{listing:basics:execution-policy-example}.

\begin{listing}[H]
\cppfile{./chapters/basics/ExecutionPolicies.cpp}
  \caption{Usage of Execution Policies}
  \label{listing:basics:execution-policy-example}
\end{listing}


\section{HPX}

HPX\cite{kaiser_hpx:_2014} (High Performance ParalleX) is a general
purpose C++ runtime system for parallel and distributed applications
of any scale\cite{ste||ar_group_hpx_2018}. HPX adheres to the C++11
standard and provides implementations for features from the
Concurrency \acrshort{ts} and the Parallelism \acrshort{ts}. HPX makes
heavy use of the Boost libraries, which makes it easy to use, highly
optimized, and very portable. HPX is the first open source
implementation of the ParalleX execution
model\cite{kaiser_parallex_2009}, which focuses on overcoming the four
main barriers to scalability\cite{ste||ar_group_hpx_2018}:

\begin{itemize}
\item{\textbf{S}tarvation: insufficient work available to maintain
high resource utilization}
\item{\textbf{L}atencies: the time-distance delay intrinsic to
accessing remote resources and services.}
\item{\textbf{O}verhead: the work required for the management of
parallel actions and resources on the critical execution path which is
not necessary in a sequential variant.}
\item{\textbf{W}aiting for contention resolution: the delay due to the
lack of availability of oversubscribed shared resources.}
\end{itemize}


HPX provides a runtime system on which C++ \mintinline{C++}{future}s
can be run. For this, HPX starts \texttt{N} \acrshort{os} threads on
which \texttt{M} HPX threads are multiplexed. \acrshort{os} threads
are kernel-space threads created through \acrshort{os}
\acrshort{api}s. For \acrshort{posix} systems, HPX uses the
pthreads\cite{ieee_draft_2008} \acrshort{posix} \acrshort{api}. HPX
threads, on the other hand, are lightweight user-space threads which
are managed by the HPX scheduler. HPX threads are cooperative in
nature and therefore preemption is not supported. HPX threads are
entirely in user-space and therefore they do not have to incur the
cost of kernel context switching.

A major advantage of user-space threads is that controlling scheduling
policies is possible with them. When using kernel-space threads, the
kernel is in charge of scheduling those threads, which makes
modifications to the scheduling policy limited or impossible. With
user-space threads, however, the user-space program is responsible for
deciding which user-space thread should run at which point in
time. HPX allows defining custom scheduling policies for HPX threads.

Upon starting, HPX uses the respective \acrshort{os} \acrshort{api}s
to start an \acrshort{os} thread for each of the \acrshort{cpu} cores
it has been allocated. From this point onward, HPX does not start any
new \acrshort{os} threads. Each of these \acrshort{os} threads runs
the HPX scheduling loop, which fetches the next HPX thread that should
be run and then runs it to completion or until it decides to yield
control back to the scheduler.

\section{Time measurement}

All time measurements in this thesis are performed using the C++
standard library \mintinline{C++}{std::chrono::steady_clock}. This
clock is guaranteed to be monotonically increasing. This prevents
errors caused by changes in the ``wall time'' of the clock, e.g. due
to changes to Daylight Savings Time or other political time
changes. The clock is not related to wall time at all. Additionally,
this clock provides nanosecond resolution on Windows, MacOS and Linux.

\section{GNU/Linux}

Throught this thesis, the GNU/Linux (also referred to simply as Linux)
will be used for running measurements. Linux is an open-source
general-purpose operating system created by Linus Torvalds in 1991,
and developed by the community ever since. In the context of this
thesis, it is important to note that Linux is not a real-time
operating systems and therefore applications running on it can not
make real-time guarantees. However, at the time of this writing, HPX
does not support any real-time operating systems, so there is no
better option than Linux for running measurements. Some deadline
misses are to be expected due to the lack of real-time support from
theoperating system.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis.tex"
%%% TeX-engine: xetex %%% End:
