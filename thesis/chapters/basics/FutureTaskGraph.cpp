int a = 1, b = 2, c = 3, d = 4;
future<int> mult1 = async([=]() { return a * b });
future<int> mult2 = async([=]() { return c * d });
future<int> sum_future =
    when_all(mult1, mult2)
        .then([](future<tuple<future<int>, future<int>>> f) {
          auto v = f.get();               // Does not block
          auto m1 = std::get<0>(v).get(); // Does not block
          auto m2 = std::get<1>(v).get(); // Does not block
          return m1 + m2;
        });
int sum = sum_future.get();
