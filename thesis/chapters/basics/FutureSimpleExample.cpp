std::future<std::vector<char>> file_bytes_future =
    read_file("example.txt");
compute_stuff(); // Do other work while the file is being
                 // read
std::vector<char> file_bytes = file_bytes_future.get();
