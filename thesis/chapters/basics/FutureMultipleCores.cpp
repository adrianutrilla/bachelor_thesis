std::future<result> f1 =
    async([]() { return expensive_computation(); });
std::future<result> f2 =
    async([]() { return expensive_computation(); });
std::future<result> f3 =
    async([]() { return expensive_computation(); });
f1.get();
f2.get();
f3.get();
