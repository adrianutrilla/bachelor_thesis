std::future<std::vector<char>> file_bytes_future =
    read_file("example.txt");
std::future<int> line_count_future = file_bytes_future.then(
    [](std::future<std::vector<char>> f) -> int {
      return count_lines(
          f.get()); // f.get() does not block here
    });
int line_count = line_count_future.get();
