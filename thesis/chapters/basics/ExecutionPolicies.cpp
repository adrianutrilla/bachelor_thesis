std::vector<int> v = {1, 2, 3, 4};
// When no policy is specified, sequential is used as the
// default
std::for_each(v.begin(), v.end(),
              [&](auto x) { do_stuff(x); });
// Execute in parallel
std::for_each(std::par, v.begin(), v.end(),
              [&](auto x) { do_stuff(x); });
