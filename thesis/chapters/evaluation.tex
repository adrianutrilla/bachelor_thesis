\chapter{Evaluation of the \acrshort{edf} Scheduler implementation}

In this chapter, the performance of the \acrshort{edf} scheduler
implemented previously will be evaluated. This includes basic overhead
testing and simulation of realistic task sets in order to evaluate the
fitness of the implementation. Unless specified otherwise, all
measurements were run in a 24-thread server running Linux.

\section{Overhead of running one empty task}

The first measurement to evaluate the performance of the
implementation is measuring the overhead of running a single task with
HPX running on a single \acrshort{cpu} core. The reason to run on only
one \acrshort{cpu} core is to ensure there is no contention around
locks. Ensuring low overhead for running tasks is very important,
because if the running of tasks is slow, the scheduler would be unfit
for situations that require low overhead, such as tasks with short
deadlines. The code for this measurement is shown in Listing
\ref{listing:evaluation:overhead-single-task}. An empty HPX thread is
started using \mintinline{C++}{hpx::async} and then immediately waited
on with \mintinline{C++}{.get()}. The time it takes to both create and
wait on the HPX thread is measured using
\mintinline{C++}{std::chrono::steady_clock}. The measurement is
repeated 10,000 times in order to get statistically useful data. In
order to execute with only one \acrshort{os} thread, the program is
started with the argument \texttt{--hpx:threads=1}.

\begin{listing}[H]
\cppfile{./chapters/evaluation/OverheadSingleTask.cpp}
  \caption{Performance measurement of running a single task}
  \label{listing:evaluation:overhead-single-task}
\end{listing}

\begin{table}[H]
\input{./chapters/evaluation/SingleTaskOverheadResults.tex}
  \caption{Times required to run a single task. All times in
nanoseconds.}
  \label{table:overhead-single-task-summary}
\end{table}

\begin{figure}[H]
\centerline{\input{./chapters/evaluation/TaskStartOverheadHistogram.pgf}}
  \caption{Histogram of times required to start a single task}
  \label{figure:evaluation:overhead-single-task-histogram}
\end{figure}


The mean time required to run the task is short, just over 3
microseconds. However, it is worth noting that for the 90\% percentile
and above, the time required increases drastically. The reason for
this is that the measurements are run on a non-real-time operating
system, in this case, Linux. The process that runs the measurements is
preempted several times by the kernel, which causes delays that creep
into the measurements. This is expected behavior when running under an
operating system that does not know about the real-time constraints of
the measurements.


\section{Matrix multiplication}

The next measurement to evaluate the usefulness of the \acrshort{edf}
scheduler is to measure whether any performance benefits arise from
parallelizing work when using the scheduler. For this, a simple matrix
multiplication algorithm was implemented, and then parallelized. The
runtime of the algorithm was the measured before and after
parallelizing. Listing \ref{listing:evaluation:matrix-multiplication}
shows the algorithm used to multiply matrices sequentially. It's worth
noting that this algorithm is very inefficient, with a runtime of
$O(n^3)$ with $n$ the size of the matrix, for square matrices. For
this performance measurement, square matrices with size 300 are
used. Each multiplication is repeated 10,000 times to ensure
statistically useful data.

\begin{listing}[H]
\cppfile{./chapters/evaluation/MatrixMultiplication.cpp}
  \caption{The matrix multiplication algorithm}
  \label{listing:evaluation:matrix-multiplication}
\end{listing}

Even though the algorithm is inefficient, it is extremely easy to
parallelize. Listing
\ref{listing:evaluation:parallel-matrix-multiplication} shows how this
algorithm was parallelized. An HPX thread is started for each row, so
that the multiplication can run in parallel. This function takes a
yield function as a parameter. The yield function is called each time
an element of the result matrix is fully computed. The measurements
are run with several different yield functions to evaluate the effects
of yielding.

\begin{listing}[H]
\cppfile{./chapters/evaluation/ParallelMatrixMultiplication.cpp}
  \caption{The parallel matrix multiplication algorithm}
  \label{listing:evaluation:parallel-matrix-multiplication}
\end{listing}

Table \ref{table:matrix-runtimes} shows the times required for each of
the different measurements. The following measurements are run:

\begin{itemize}
\item{\texttt{sequential}: sequential matrix multiplication as shown
in Listing \ref{listing:evaluation:matrix-multiplication}.}
\item{\texttt{parallel}: parallel matrix multiplication with an empty
yield function, so that the HPX threads never yield to the scheduler.}
\item{\texttt{parallel\_yield}: parallel matrix multiplication with a
yield function that always yields to the scheduler.}
\item{\texttt{parallel\_yield\_maybe}: parallel matrix multiplication
using the \mintinline{C++}{yield_maybe} function shown in Listing
\ref{listing:implementation:yieldmaybe}.}
\item{\texttt{parallel\_yield\_maybe\_synchronized}: parallel matrix
multiplication using the \mintinline{C++}{yield_maybe} function shown
in Listing \ref{listing:implementation:yieldmaybe} that additionally
synchronizes with the scheduler's global lock to avoid races.}
\end{itemize}

\begin{table}[H] \input{./chapters/evaluation/MatrixRuntimes.tex}
  \caption{Times required to multiply two 300x300 matrices. Times in
microseconds.}
  \label{table:matrix-runtimes}
\end{table}

The \texttt{parallel} multiplication algorithm is indeed faster than
\texttt{sequential} multiplication, in particular more than
\input{./chapters/evaluation/MatrixParallelSpeedup.tex} times faster
than the sequential algorithm. This shows that work can be performed
faster with HPX and the \acrshort{edf} scheduler by parallelizing it
rather than running it sequentially.

However, the \texttt{parallel} algorithm never yields to the
scheduler, which would lead to deadline misses in real-world
workloads. The \texttt{parallel\_yield} algorithm, which always
yields, is
\input{./chapters/evaluation/MatrixParallelYieldSlowdown.tex} times
slower than \texttt{parallel}, which makes it even slower than
\texttt{sequential}. The performance impact of always yielding is far
too high.

The \texttt{parallel\_yield\_maybe} algorithm uses
\mintinline{C++}{yield_maybe} to avoid yielding unnecessarily, and
shows a much milder performance impact over not yielding at all, with
only a
\mbox{\input{./chapters/evaluation/MatrixParallelYieldMaybeSlowdown.tex}\%}
slowdown compared to \texttt{parallel}. If the additional correctness
of \texttt{parallel\_yield\_maybe\_synchronized} is desired, it comes
at a cost of a
\mbox{\input{./chapters/evaluation/MatrixParallelYieldMaybeSynchronizedSlowdown.tex}\%}
slowdown when running on 24 threads compared to \texttt{parallel}.

\section{\acrshort{cpu} core scalability}

In order to obtain speedups from the parallelization of code, such
code must run on multi-core systems. Because HPX allows assigning more
than one \acrshort{cpu} core to a single scheduler, evaluating the
impact of assigning more and more \acrshort{cpu} cores to a single
scheduler will assess the scalability of the scheduler with the number
of \acrshort{cpu} cores.

In order to measure the scalability, the overhead for running tasks
will be measured. For this measurement, $n$ tasks that each busy-wait
for 100 microseconds will be started simultaneously, with $n$ being
the number of OS threads HPX is allocated. Then, HPX will wait until
all the $n$ tasks are done. The time it takes from just before the
tasks are started to when the tasks are done will be recorded. The
overhead is then defined as the difference between the recorded time
and the time that each task busy-waits for. Listing
\ref{listing:evaluation:number-of-cores-overhead-measurement} shows
the source code for the measurement.

\begin{listing}[H]
\cppfile{./chapters/evaluation/NumberOfCoresOverheadMeasurement.cpp}
  \caption{\acrshort{cpu} core overhead measurement code}
  \label{listing:evaluation:number-of-cores-overhead-measurement}
\end{listing}

For this measurement, a 64 thread virtual machine on Google Compute
Engine was used. Using a virtual machine can have performance impacts,
but it allows running the measurement on \acrshort{cpu}s with high
thread count which were not available to me without
virtualization. The measurement was started with a single
\acrshort{os} thread, and then was repeated, adding one OS thread each
run until the number of \acrshort{os} threads was equal to the number
of available concurrent threads on the \acrshort{cpu}. Each
measurement was repeated 10,000 times. The measurements were run using
two different lock types for the \acrshort{edf} scheduler, one with
\mintinline{C++}{std::mutex}, and then with
\mintinline{C++}{hpx::util::spinlock}. The main difference between
these is that \mintinline{C++}{std::mutex} requires kernel arbitration
for contention resolution, whereas
\mintinline{C++}{hpx::util::spinlock} is entirely user-space. Figure
\ref{figure:evaluation:overhead-measurements} shows the results of
these measurements. It is clearly apparent that with high
\acrshort{cpu} counts, the overhead increases quadratically with the
number of \acrshort{cpu} threads.

\begin{figure}[H]
\centerline{\input{./chapters/evaluation/measurements.pgf}}
  \caption{Graph of the overhead measurements}
  \label{figure:evaluation:overhead-measurements}
\end{figure}

The primary suspect for quadratic behavior for this measurement is
lock contention. It's important to remember that the \acrshort{edf}
scheduler has a single global lock, and that therefore no more than
one thread can call a scheduler function at the same time. Because $n$
HPX threads are being started almost simultaneously, $n$ threads are
trying to call the scheduler at the same time. All $n$ threads try to
unlock the global lock at the same time, which creates contention.

Using a profiler, it is possible to see where any given process is
spending its processing time. If the quadratic behavior is caused by
lock contention, then the profiler should show a high percentage of
the process's time is spent waiting on a lock. \texttt{perf} is a
powerful profiler for Linux that includes profiling of many
kernel-level events as well as a sampling profiler for user code. The
profiler was executed on the same machine as the measurements were run
with the following command: \texttt{sudo perf record
./number\_of\_cores\_overhead --locktype spinlock}. This creates an
output file called \texttt{perf.data} containing all the profile
samples for the measurement using
\mintinline{C++}{hpx::util::spinlock}.

The profile samples can then be analyzed with the command \texttt{perf
report}. The output of this command shows that 50\% of the time is
spent on the function
\mintinline{C++}{boost::detail::spinlock::try_lock}, which is indeed
used for locking. The results for the measurement using
\mintinline{C++}{std::mutex} support this theory even further, showing
over 90\% of the time being spent inside the kernel function
\mintinline{C++}{native_queued_spin_lock_slowpath}, which is also
related to locking. The higher time spent on locks for the
\mintinline{C++}{std::mutex} case is also reflected in Figure
\ref{figure:evaluation:overhead-measurements}, as the times for that
case are consistently higher than those of
\mintinline{C++}{hpx::util::spinlock}, due to the need to context
switch to the kernel to resolve contention.

Looking closely at the results, it is apparent that for low
\acrshort{cpu} thread counts, the overhead increases linearly. This
happens until around 25 \acrshort{cpu} threads, and from then on,
overhead increases quadratically. Figure
\ref{figure:evaluation:overhead-measurements-linear} shows the same
results as Figure \ref{figure:evaluation:overhead-measurements}, but
only includes the results up to 25 \acrshort{cpu} threads. Here, one
can see that the increase in overhead is indeed linear.

\begin{figure}[H]
\centerline{\input{./chapters/evaluation/measurements_linear.pgf}}
  \caption{Graph of the linear part of the overhead measurements}
  \label{figure:evaluation:overhead-measurements-linear}
\end{figure}


Because embedded systems are the main focus of this thesis, and they
rarely have extremely high \acrshort{cpu} thread counts, the quadratic
behavior should not be problematic. Additionally, this workload
exercises the scheduler as much as possible and is therefore
unrealistic, and real workloads are likely to see much better scaling,
as they'll have less contention.


\section{Task set simulation}

The last measurement for evaluation of the \acrshort{edf} scheduler is
running a realistic workload on the scheduler. A realistic task set
was designed for this purpose. The tasks in the task set are periodic,
and perform a constant amount of work each time they are activated,
yielding to the scheduler by calling the \mintinline{C++}{yield_maybe}
function every 25 microseconds. The task set is designed to run on
three \acrshort{cpu} cores. An additional fourth \acrshort{cpu} core
is used to run timers that trigger the activation of each task
according to its period. In order to assign tasks to \acrshort{cpu}
cores, the HPX resource partitioner is used to create three thread
pools. Each of these thread pools is assigned a single \acrshort{cpu}
core, and each thread pool runs an instance of the \acrshort{edf}
scheduler.

Each task in the task set was given a name, an activation period, a
thread pool on which it runs, a work time, a relative deadline and a
static priority. Even though the \acrshort{edf} scheduler's primary
use is with deadlines, embedded system task sets are sometimes
designed with static priorities. To show that the implemented
scheduler can support this use case, the task set's tasks each have
both a priority and a relative deadline. Table \ref{table:task-set}
describes the tasks in the task set. The task set will be simulated
twice, one time using exclusively relative deadlines, and after that,
using exclusively static priorities. The simulation is run for 60
seconds.

\input{./chapters/evaluation/Tasks.tex}

The utilization per pool (and thus per \acrshort{cpu} core) is shown
in Table \ref{table:task-set-utilization}. All pools have an
utilization less than 1, therefore they should all be schedulable
using \acrshort{edf}.

\begin{table}[H] \centering
\input{./chapters/evaluation/TaskSetUtilization.tex}
  \caption{Total utilization of the task set per pool}
  \label{table:task-set-utilization}
\end{table}

The results of the simulation using deadlines are shown in Table
\ref{table:task-set-deadline-results}. The Activations column shows
how many times each task was run. The Deadline misses column shows the
number of times the deadline for a given task was missed. The table
shows that some tasks do miss their deadlines occasionally. Tasks with
shorter deadlines appear to miss their deadline more often. However,
some deadline misses are to be expected, as the simulation is run on a
non-real-time operating system. The number of missed deadlines is
small enough to be sure that the misses are not caused by a flaw in
the scheduler, because if this was the case, there should be many more
deadline misses. The Lateness column shows the mean lateness of all
activations. Lateness is defined as the difference between the
deadline for an activation of a given task and the time point when
that activation has finished executing. All values in the table for
this column are negative, as almost all deadlines are met. The Startup
Time column shows the mean time it takes from task activation (started
by the periodic timer) until the task starts executing. The results
show that tasks with short relative deadlines have shorter startup
times than those with longer deadlines. This is to be expected, as
tasks with shorter deadlines will be scheduled before tasks with
longer deadlines, therefore they will start executing earlier. Lastly,
the Inter-arrival Time column shows the mean time between every two
successive activations of each task.

\input{./chapters/evaluation/TaskSetDeadlineResults.tex}

The results of the simulation using priorities are shown in Table
\ref{table:task-set-priority-results}. Even though these tasks don't
really have deadlines (they use static priorities), in order to
evaluate deadline misses and lateness, the deadline values from the
previous simulation were used. The results are as satisfactory as
those of the deadline simulation. There are also some deadline misses,
but those are attributed to running the simulation on a non-real-time
operating system.

\input{./chapters/evaluation/TaskSetPriorityResults.tex}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis.tex"
%%% TeX-engine: xetex %%% End:
